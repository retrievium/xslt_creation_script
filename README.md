The script `fromCMLExamplesmd.py` generates a `slurp_al_chemical_to_solr.xslt`
and a `fields.rpi` to enable retrivium-portal to access all the fields which are
indexed by Solr.  

The script takes as an argument, the `CML Examples.md` file from
the retrievium wiki and the header and tail files from this repository.
The header file contains the first 21 lines from the original 
`slurp_all_chemicalML_to_solr.xslt`.  Plus lines added for tokenizing numerical
fields.  A tail file must also be in the 
directory of fromCMLExamplesmd.py and contains every line after and
 including `<!--Writes a Solr field. -->` line from the original
`slurp_all_chemicalML_to_solr.xslt` to have a 75 line file.

Put the new `slurp_all_chemicalML_to_solr.xslt` in place in
`/usr/local/fedora/tomcat/webapps/fedoragsearch/WEB-INF/classes/fgsconfigFinal/index/FgsIndex/islandora_transforms/` 
and `fields.rpi` in your retrievium-portal directory `retrievium/settings/fields.rpi`.

With the new `slurp_all_chemicalML_to_solr.xslt` in place it is a good idea to 
clear the islandora caches. Either navigate to  http://localhost:8080/admin/config/development/performance and click the Clear all caches button or  use

```console
$ drush -r /var/www/drupal7/ cache-clear all
```
This does not remove the solr index.   
```console
$ drush -r /var/www/drupal7/ search-index
or
$ drush -r /var/www/drupal7/ search-reindex
```
may enable building the solr index but I am unsure.

## Logic of fromCMLExamplemd.py script

### Read CML Examples.md:

If there are `xpath`, `data_type`, `unit`, `attributes`,
`rp_field_name` and `solr_type` headers in `CML Examaples.md` put its
value in a variable.  When all these headers are present continue on to the
logic to create the styleseet and the rpi files.  A link is also grabbed if
available and printed in the `fields.rpi`.

The six necessary headers are:

 * `xpath`         : XPATH to the element that is to be slurped by solr
 * `data_type`     : the data type of that element
 * `unit`          : the type of data stored in the CML
 * `attributes`    : a list of attributes of the element to be indexed
 * `rp_field_name` : the name of the field that rp will use
 * `solr_type`     : not really sure about the necessity of this one 
    but it was in the original `slurp_all_chemicalML_to_solr.xslt`
    and is used to specify whether a value can have multiple values or not.

First the name used to index the field in Solr is produced from the script based
on `rp_field_name`, `solr_type`, and `data_type`. If the `data_type` is date the 
script writes the corresponding lines of code to the stylesheet.  All other
`data_types` are similar and are written in the `slurp_all_chemical_to_solr.xslt`
in the same manner.  The function `field_with_attributes` adds the lines
necessary to slurp the attributes of the element that are in the attributes
variable found in `CML Examples.md`.  
