from __future__ import print_function
import re
import argparse

parser = argparse.ArgumentParser(
    description='Generate slurp_all_chemicalML_to_solr.xslt and fields.rpi ' +
                'from  data in CML_Examples.md.')
parser.add_argument('infile', metavar='FILE', type=argparse.FileType('r'), 
                   help='The "CML Examples.md" file')

# parser.add_argument("-o", dest='outfile', metavar='FILE', type=argparse.FileType('w'), 
#                    default='slurp_all_chemicalML_to_solr.xslt', help='The "style sheet" file')

#files to write: slurp_all_chemicalML_to_solr.xslt, fields.rpi
header = open('header')
slurp = open('slurp_all_chemicalML_to_solr.xslt', 'w')
for line in header:
  if line.startswith('#'):
      continue
  else:
      slurp.write(line)

RP_fields_file = open('fields.rpi', 'w')
RP_fields_comment = '#RP_name, RP_type, Solr field_name, unit \n'
RP_fields_file.write(RP_fields_comment)

#get all the information needed from CML\ Examples.md
linkRE = re.compile(r'\s*\*\s*\<(http\://www.*)\>', re.IGNORECASE)
xpathRE = re.compile(r'\s*\*\s*XPATH:\s*`(.*)`\s*$', re.IGNORECASE)
attributesRE = re.compile(r'\s*\*\sAttributes:\s(.*)\s*$', re.IGNORECASE)
data_typeRE = re.compile(r'\s*\*\s*\s*R?P?\s?Data\s?Type:\s*(?:xsd\:)?(\S*|N/A)\s*$', re.IGNORECASE)
rp_field_nameRE = re.compile(r'\s*\*\s*RP\s?field:\s*(`?)(\S*)(\1)\s*$', re.IGNORECASE )
solr_typeRE = re.compile(r'\s*\*\s*Solr\s?type:\s*(`?)(\w*)(\1)\s*$', re.IGNORECASE)
unitRE = re.compile(r'\s*\*\s*Unit\s?Type:\s+(?:nonSi|si)?\:?(\S*|N/A|\S* ?\S*)?\s*$', re.IGNORECASE)

matrix_typeRE = re.compile(r'matrixType')

def field_with_attributes(rp_field_name, solr_cml, attributes):
    """ A function to add fields to the xslt for the attributes from a CML element
        if needed """
    original_solr_cml = solr_cml
    for attribute in attributes:
        if attribute == 'False':
            continue

        if attribute != ".":
            attribute = '@' + attribute
        if attribute == '.':
            # check if the value is numeric
            if re.match(r'.*_m?[di]$',original_solr_cml):
                slurp.write("""    <xsl:apply-templates mode="writing_cml_field" select="str:tokenize((.),' ')">\n""")
            else:
                slurp.write("""    <xsl:apply-templates mode="writing_cml_field" select=".">\n""")
            slurp.write("""      <xsl:with-param name="field_name">""" + \
                           original_solr_cml + """</xsl:with-param>\n""")
            slurp.write("""    </xsl:apply-templates>\n""")
            # write fields.rpi

        elif data_type != 'date':
            insert_attribute_name = re.match(r'(\w+)(_\w+)', original_solr_cml)
            solr_cml = insert_attribute_name.group(1) + '_' \
                       + attribute[1:] + insert_attribute_name.group(2)
            if attribute == '@columns' or attribute == '@rows' or \
                attribute  == '@size':
                solr_cml =  insert_attribute_name.group(1) + '_' \
                            + attribute[1:] + '_mi'
                rp_field_name_for_file = rp_field_name + '_' + attribute[1:]
                RP_type = 'int'
            if attribute == '@units' or attribute == '@matrixType' or \
               attribute == '@convention':
               solr_cml =  insert_attribute_name.group(1) + '_' \
                           + attribute[1:] + '_ms'
               rp_field_name_for_file = rp_field_name + '_' + attribute[1:]
               RP_type = 'str'

            slurp.write('    <xsl:apply-templates mode="writing_cml_field" select="')
            line = attribute + '">\n'
            slurp.write(line)
            slurp.write("""      <xsl:with-param name="field_name">""" + solr_cml + """</xsl:with-param>\n""")
            slurp.write("""    </xsl:apply-templates>\n""")
            if re.match(r'.*_m?[di]$',original_solr_cml):
                RP_type = 'list(' + RP_type +')'
            rp_line = rp_field_name_for_file + ', ' + RP_type + ', ' + 'cml_' +  \
                      solr_cml +',dimensionless' + ', no link' + '\n'
            rp_line = rp_line.replace('matrixType', 'matrix_type',1)
            RP_fields_file.write(rp_line)

args = parser.parse_args()
with args.infile as mdfile:
    for line in mdfile:
        if linkRE.match(line):
            link = linkRE.match(line)
            continue


        if  xpathRE.match(line):
            xpath = xpathRE.match(line)
            xpath_for_xslt = xpath.group(1)
            xpath_for_xslt = re.sub('cml:', 'cmls:', xpath_for_xslt)
            xpath_for_xslt = re.sub('^./', "", xpath_for_xslt)
            xpath_for_xslt = re.sub("'", '"', xpath_for_xslt)
            continue

        if data_typeRE.match(line):
            data_type = data_typeRE.match(line).group(1)
            continue
        
        if unitRE.match(line):
            unit = unitRE.match(line).group(1)
            continue
    
        if attributesRE.match(line):
            attributes = [x.strip() for x in attributesRE.match(line).group(1).split(',')]
            continue
        
        if rp_field_nameRE.match(line):
            rp_field_name = rp_field_nameRE.match(line).group(2)
            continue
        
        if solr_typeRE.match(line):
            solr_type = solr_typeRE.match(line).group(2)
            continue
        

        try:
            xpath_for_xslt, data_type, unit, attributes, rp_field_name, solr_type
        except NameError:
            continue

        # This mess can be simplifie using substring of data_type and solr_type 
        if data_type == 'double' and solr_type == 'single_valued':
            solr_cml = rp_field_name + '_d'
        elif data_type == 'double' and solr_type == 'multi_valued':
            solr_cml = rp_field_name + '_md'
        elif data_type == 'string' and solr_type == 'single_valued':
            solr_cml = rp_field_name + '_s'
        elif data_type == 'string' and solr_type == 'multi_valued':
            solr_cml = rp_field_name + '_ms'
        elif data_type == 'integer' and solr_type == 'single_valued':
            solr_cml = rp_field_name + '_i'
        elif data_type == 'integer' and solr_type == 'multi_valued':
            solr_cml = rp_field_name + '_mi'
        elif data_type == 'date' and solr_type == 'single_valued':
            solr_cml = rp_field_name + '_dt'
        elif data_type == 'date' and solr_type == 'multi_valued':
            solr_cml = rp_field_name + '_mdt'
        else:
        # Added this for input molecule.  Because its data_type was N/A
            solr_cml = rp_field_name + '_ms'

        slurp.write('\n')
        if data_type == 'date':
            solr_cml = rp_field_name + '_mdt'
            slurp.write("""<xsl:template match=""" + "'" + xpath_for_xslt + "'" + """ mode="writing_cml">\n""")
            slurp.write("""  <xsl:variable name="textValue">\n""")
            slurp.write("""    <xsl:call-template name="get_ISO8601_date">\n""")
            slurp.write("""      <xsl:with-param name="date" select="."/>\n""")
            slurp.write("""    </xsl:call-template>\n""")
            slurp.write("""  </xsl:variable>\n""")
            slurp.write("""  <xsl:apply-templates mode="writing_cml_field" select=".">\n""")
            slurp.write("""    <xsl:with-param name="field_name">""" + solr_cml + """</xsl:with-param>\n""")
            slurp.write("""    <xsl:with-param name="value">\n""")
            slurp.write("""      <xsl:value-of select="$textValue"/>\n""")
            slurp.write("""    </xsl:with-param>\n""")
            slurp.write("""  </xsl:apply-templates>\n""")
            slurp.write("""</xsl:template>\n""")
        else:
            slurp.write("""  <xsl:template match=""" + "'" +  xpath_for_xslt + "'" + """ mode="writing_cml">\n""")
            if solr_type == 'single_valued':
                    slurp.write("""<xsl:if test="java:add($single_valued_hashset_for_cml,'""")
                    slurp.write(solr_cml)
                    slurp.write("""')">\n""")
            # Placing the "." in attributes allows the element of the attributes to
            # be selected.  
            if "." not in attributes:
                attributes.append('.')
            field_with_attributes(rp_field_name, solr_cml, attributes)

            if solr_type == 'single_valued':
                slurp.write("""</xsl:if>\n""")
            slurp.write("""  </xsl:template>\n""")
            
            #
            #write fields.rpi for RP
            #
            if data_type == 'string':
                    RP_type = 'str'
            elif data_type == 'double':
                    RP_type = 'float'
            elif data_type == 'integer':
                    RP_type = 'int'
            elif data_type == 'date':
                    RP_type = 'datetime'
            # if the data_type is N/A it will just be indexed as a string
            elif data_type == 'N/A':
                    RP_type = 'str'
            
            if solr_type == 'multi_valued':
                    RP_type = 'list(' + RP_type + ')'
            
            if  unit == 'none':
                RP_line = rp_field_name.lower() + ', '  + RP_type + ', ' \
                        + 'cml_' + solr_cml + ', None'
            else:
                RP_line = rp_field_name.lower() + ', ' + RP_type + ', ' \
                        + 'cml_' + solr_cml + ', ' + unit
            try:
                RP_line = RP_line + ',', link.group(1)
            except NameError:
                RP_line = RP_line + ',' + 'TODO'

            fields_line = ''
            for item in RP_line:
                fields_line = fields_line + item
            RP_fields_file.write('%s \n' % fields_line)
            del RP_line
        del xpath_for_xslt, rp_field_name, solr_cml

        try:
            del link
        except NameError:
            pass
        
tail = open('tail')
for line in tail:
  slurp.write(line)

tail.close()
slurp.close()
